<?php

namespace QBNK\QBank\Protocol\Controller;

use QBNK\QBank\Protocol\Model\Aws;
use QBNK\QBank\Protocol\Model\Azure;
use QBNK\QBank\Protocol\Model\Copy;
use QBNK\QBank\Protocol\Model\Dropbox;
use QBNK\QBank\Protocol\Model\Ftp;
use QBNK\QBank\Protocol\Model\GoogleCloudStorage;
use QBNK\QBank\Protocol\Model\ProtocolAbstract;
use QBNK\QBank\Protocol\Model\Sftp;

class ProtocolController {

	/**
	 * @return ProtocolAbstract[]
	 */
	public static function getProtocols() {
		return [
			Copy::NAME => new Copy(),
			Ftp::NAME => new Ftp(),
			Sftp::NAME => new Sftp(),
			Azure::NAME => new Azure(),
			Dropbox::NAME => new Dropbox(),
			Aws::NAME => new Aws(),
			GoogleCloudStorage::NAME => new GoogleCloudStorage()
		];
	}
}