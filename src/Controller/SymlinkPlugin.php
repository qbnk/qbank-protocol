<?php

namespace QBNK\QBank\Protocol\Controller;

use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use League\Flysystem\FilesystemInterface;
use League\Flysystem\PluginInterface;

class SymlinkPlugin implements PluginInterface {
	/**
	 * @var Filesystem
	 */
	protected $filesystem;

	public function setFilesystem(FilesystemInterface $filesystem) {
		$this->filesystem = $filesystem;
	}

	public function getMethod() {
		return 'symlink';
	}

	public function handle($sourceFile, $linkName) {
		/** @var Local $localAdapter */
		$localAdapter = $this->filesystem->getAdapter();

		if (!file_exists($sourceFile)) {
			throw new \Exception('Source "'.$sourceFile.'" doesnt exist, cannot symlink');
		}

		$localAdapter->createDir(dirname($linkName), $this->filesystem->getConfig());

		//cannot overwrite symlinks, must unlink first
		if (is_link($localAdapter->applyPathPrefix($linkName))) {
			unlink($localAdapter->applyPathPrefix($linkName));
		} else if ($this->filesystem->has($linkName)) {
			throw new \Exception('Target "'.$localAdapter->applyPathPrefix($linkName).'" already exists, cannot symlink');
		}

		if (!symlink($sourceFile, $localAdapter->applyPathPrefix($linkName))) {
			throw new \Exception('Could not link "'.$sourceFile.'" to "'.$localAdapter->applyPathPrefix($linkName).'"');
		}

		return true;
	}
}