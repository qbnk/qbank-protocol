<?php

namespace QBNK\QBank\Protocol\Model;

use League\Flysystem\AdapterInterface;

abstract class ProtocolAbstract implements \JsonSerializable {

	const NAME = '';
	const DESCRIPTION = '';

	public function __construct($parameters = []) {
		// Implement me
	}

	/**
	 * Verify that this protocol configuration is valid by endpoint service. Return true or string with error message
	 * @return true|string
	 */
	public function validateConnection() {
		return true;
	}

	/**
	 * @return array
	 */
	public function getProperties() {
		return [];
	}

	/**
	 * @return AdapterInterface
	 */
	public function getFlySystemAdapter() {
		return null;
	}

	/**
	 * @return string
	 */
	public function getDescription() {
		return $this::DESCRIPTION;
	}

	/**
	 * @return string
	 */
	public function getName() {
		return $this::NAME;
	}

	/**
	 * Flysystem Adapters handle base paths differently, so these two sibling methods exist to make sure that we apply paths correctly in the right situation
	 * $return string
	 */
	public function preparePath($filename) {
		return $this->applyPath($filename);
	}

	/**
	 * Flysystem Adapters handle base paths differently, so these two sibling methods exist to make sure that we apply paths correctly in the right situation
	 * $return string
	 */
	public function applyPath($filename) {
		return $filename;
	}

	/**
	 * Gets all data that should be available in a json representation.
	 *
	 * @return \stdClass
	 */
	public function jsonSerialize() {
		$protocol = new \stdClass();
		$protocol->name = $this->getName();
		return $protocol;
	}

	/**
	 * @param array $protocol
	 * @return ProtocolAbstract
	 */
	public static function fromJson(array $protocol) {
		switch ($protocol['name']) {
			case Copy::NAME:
				return new Copy($protocol);
			case Ftp::NAME:
				return new Ftp($protocol);
			case Sftp::NAME:
				return new Sftp($protocol);
			case Azure::NAME:
				return new Azure($protocol);
			case Dropbox::NAME:
				return new Dropbox($protocol);
			case Aws::NAME:
				return new Aws($protocol);
			case GoogleCloudStorage::NAME:
				return new GoogleCloudStorage($protocol);
			default:
				throw new \RuntimeException(
					'Unknown protocol class: "'.$protocol['name'].'"'
				);
		}
	}
}