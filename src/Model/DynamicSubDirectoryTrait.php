<?php

namespace QBNK\QBank\Protocol\Model;

trait DynamicSubDirectoryTrait {

	/**
	 * @var int
	 */
	protected $subDirectoryDepth;

	/**
	 * @var string
	 */
	protected $subDirectoryType;

	/**
	 * @return string
	 */
	public function getSubDirectoryType() {
		return $this->subDirectoryType;
	}

	/**
	 * @param string $subDirectoryType
	 * @return $this
	 */
	public function setSubDirectoryType($subDirectoryType) {
		$this->subDirectoryType = $subDirectoryType;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getSubDirectoryDepth() {
		return $this->subDirectoryDepth;
	}

	/**
	 * @param int $subDirectoryDepth
	 * @return DeployJob
	 */
	public function setSubDirectoryDepth($subDirectoryDepth) {
		$this->subDirectoryDepth = (int)$subDirectoryDepth;
		return $this;
	}

	public function dynamicSubDirectoryConstruct(array $params = null) {
		if (isset($params[self::SYSTEMNAME_SUBDIR_DEPTH])) {
			$this->subDirectoryDepth = (int)$params[self::SYSTEMNAME_SUBDIR_DEPTH];
		}
		if (isset($params[self::SYSTEMNAME_SUBDIR_TYPE])) {
			$this->subDirectoryType = $params[self::SYSTEMNAME_SUBDIR_TYPE];
		}
	}

	public function getDynamicSubDirectoryProperties() {
		return [
			[
				'name' => 'Dynamic subdirectory pattern',
				'systemname' => self::SYSTEMNAME_SUBDIR_TYPE,
				'description' => 'Decide how optional dynamic subdirectories will be named.',
				'datatype_id' => 6,
				'definition' => [
					'options' => [self::SUBDIR_TYPE_MEDIAID, self::SUBDIR_TYPE_HASH],
					'array' => true,
					'mandatory' => true
				]
			],
			[
				'name' => 'Dynamic subdirectory depth',
				'systemname' => self::SYSTEMNAME_SUBDIR_DEPTH,
				'description' => 'Optional depth of dynamic subdirectories to append to target directory on publish.',
				'datatype_id' => 5,
			]
		];
	}

	public function getDynamicSubDirectoryJson(\stdClass $json) {
		$json->{self::SYSTEMNAME_SUBDIR_TYPE} = $this->getSubDirectoryType();
		$json->{self::SYSTEMNAME_SUBDIR_DEPTH} = $this->getSubDirectoryDepth();
		return $json;
	}
}