<?php

namespace QBNK\QBank\Protocol\Model;

use League\Flysystem\Azure\AzureAdapter;
use MicrosoftAzure\Storage\Blob\BlobRestProxy;
use MicrosoftAzure\Storage\Common\ServiceException;
use MicrosoftAzure\Storage\Common\ServicesBuilder;

class Azure extends ProtocolAbstract {
	const NAME = 'Azure';
	const DESCRIPTION = 'Publish files to an Azure container';

	/**
	 * @var string
	 */
	protected $accountName;

	/**
	 * @var string
	 */
	protected $accountKey;

	/**
	 * @var string
	 */
	protected $container;

	public function __construct(array $parameters = []) {
		parent::__construct($parameters);

		if (isset($parameters['protocol_accountname'])) {
			$this->accountName = $parameters['protocol_accountname'];
		}
		if (isset($parameters['protocol_accountkey'])) {
			$this->accountKey = $parameters['protocol_accountkey'];
		}
		if (isset($parameters['protocol_container'])) {
			$this->container = $parameters['protocol_container'];
		}
	}

	public function validateConnection() {
		try {
			$this->getBlobService()->getContainerProperties($this->getContainer());
		} catch (ServiceException $se) {
			return $se->getMessage();
		}

		return true;
	}

	public function getProperties() {
		return [
			[
				[
					'name' => 'Account name',
					'systemname' => 'protocol_accountname',
					'datatype_id' => 6,
					'definition' => [
						'mandatory' => true,
					],
				],
				[
					'name' => 'Account key',
					'systemname' => 'protocol_accountkey',
					'datatype_id' => 6,
					'definition' => [
						'mandatory' => true,
					],
				],
				[
					'name' => 'Container',
					'systemname' => 'protocol_container',
					'datatype_id' => 6,
					'definition' => [
						'mandatory' => true,
					],
				],
			],
		];
	}

	/**
	 * @return string
	 */
	public function getAccountName() {
		return $this->accountName;
	}

	/**
	 * @return string
	 */
	public function getAccountKey() {
		return $this->accountKey;
	}

	/**
	 * @return string
	 */
	public function getContainer() {
		return $this->container;
	}

	/**
	 * @return AzureAdapter
	 */
	public function getFlySystemAdapter() {
		return new AzureAdapter($this->getBlobService(), $this->getContainer());
	}

	public function jsonSerialize() {
		$json = parent::jsonSerialize();
		$json->protocol_accountname = $this->getAccountName();
		$json->protocol_accountkey = $this->getAccountKey();
		$json->protocol_container = $this->getContainer();
		return $json;
	}

	/**
	 * @return BlobRestProxy
	 */
	private function getBlobService() {
		return ServicesBuilder::getInstance()->createBlobService(
			sprintf(
				'DefaultEndpointsProtocol=https;AccountName=%s;AccountKey=%s',
				$this->getAccountName(),
				$this->getAccountKey()
			)
		);
	}
}