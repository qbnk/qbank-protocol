<?php


namespace QBNK\QBank\Protocol\Model;

use Google\Cloud\Storage\StorageClient;
use Superbalist\Flysystem\GoogleStorage\GoogleStorageAdapter;

class GoogleCloudStorage extends Copy {

	public const NAME =  'Google Cloud Storage';
	public const DESCRIPTION = 'Copies the published files to a Google bucket storage.';

	/** @var string */
	protected $bucket;

	/** @var array */
	protected $credentials;

	public function __construct(array $parameters = []) {
		parent::__construct($parameters);

		if (isset($parameters['credentials'])) {
			if (\is_string($parameters['credentials'])) {
				$this->credentials = json_decode($parameters['credentials'], true);
			} else if (\is_array($parameters['credentials'])) {
				$this->credentials = $parameters['credentials'];
			}
		}

		if (isset($parameters['bucket'])) {
			$this->bucket = $parameters['bucket'];
		}
	}

	/**
	 * @return bool|string
	 */
	public function validateConnection() {
		try {
			return $this->getStorageClient()->bucket($this->bucket)->exists();
		} catch (\Throwable $e) {
			return $e->getMessage();
		}
	}

	/**
	 * @return array
	 */
	public function getProperties(): array {
		$propertySets = array_merge([
			[
				[
					'name' => 'Credentials',
					'systemname' => 'credentials',
					'description' => 'The credentials of the service account can be obtained via the Google developer console.',
					'datatype_id' => 6,
					'definition' => [
						'stringformat' => 'json',
						'multiline' => true,
						'mandatory' => true,
					],
				],
				[
					'name' => 'Bucket name',
					'systemname' => 'bucket',
					'description' => 'Where assets should be stored.',
					'datatype_id' => 6,
					'definition' => [
						'mandatory' => true,
					],
				]
			],
		],
			parent::getProperties());

		// Sub dir is not mandatory for Google Storage
		foreach ($propertySets as $key => $propertyset) {
			foreach ($propertyset as $subKey => $property) {
				if ($property['systemname'] === self::SYSTEMNAME_SUBDIR) {
					unset($propertySets[$key][$subKey]['definition']['mandatory']);
				}
			}
		}

		return $propertySets;
	}

	/**
	 * @return \League\Flysystem\Adapter\Local|GoogleStorageAdapter
	 */
	public function getFlySystemAdapter() {
		$client = $this->getStorageClient();
		return new GoogleStorageAdapter($client, $client->bucket($this->bucket), $this->getSubDirectory());
	}

	/**
	 * @return StorageClient
	 */
	private function getStorageClient(): StorageClient {
		return new StorageClient(['keyFile' => $this->getCredentials()]);
	}

	/**
	 * @return string
	 */
	public function getBucket(): string {
		return $this->bucket;
	}

	/**
	 * @return array
	 */
	public function getCredentials(): array {
		return $this->credentials;
	}

	/**
	 * @return \stdClass
	 */
	public function jsonSerialize(): \stdClass {
		$json = parent::jsonSerialize();
		$json->credentials = json_encode($this->getCredentials());
		$json->bucket = $this->getBucket();

		return $json;
	}
}