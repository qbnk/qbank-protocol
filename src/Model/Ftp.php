<?php

namespace QBNK\QBank\Protocol\Model;

class Ftp extends Copy {
	public const NAME = 'FTP';
	public const DESCRIPTION = 'Connects to a FTP-server and transfers the published files there.';
	public const CONNECTION_RETRIES = 5;

	/** @var string */
	protected $protocol_hostname;

	/** @var int */
	protected $protocol_port;

	/** @var string */
	protected $protocol_username;

	/** @var string */
	protected $protocol_password;

	/** @var string */
	protected $protocol_file_permissions;

	public function __construct(array $parameters = []) {
		parent::__construct($parameters);

		if (isset($parameters['protocol_hostname'])) {
			$this->protocol_hostname = $parameters['protocol_hostname'];
		}
		if (isset($parameters['protocol_port'])) {
			$this->protocol_port = $parameters['protocol_port'];
		}
		if (isset($parameters['protocol_username'])) {
			$this->protocol_username = $parameters['protocol_username'];
		}
		if (isset($parameters['protocol_password'])) {
			$this->protocol_password = $parameters['protocol_password'];
		}
		if (isset($parameters['protocol_file_permissions'])) {
			$this->protocol_file_permissions = $parameters['protocol_file_permissions'];
		}
	}

	protected function isOctal($x) {
		return decoct(octdec($x)) == $x;
	}

	public function validateConnection() {
		$errors = [];
		for ($i = 1; $i <= self::CONNECTION_RETRIES; $i++) {
			try {
				$this->getFlySystemAdapter()->connect();
				return true;
			} catch (\Exception $e) {
				$errors[] = $e->getMessage();
				sleep($i**2); // backoff
			}
		}

		return sprintf(
			'Connection failed after %d attempts; Errors: %s',
			$i,
			implode('; ', $errors)
		);
	}

	/**
	 * @return string
	 */
	public function getHostname() {
		return $this->protocol_hostname;
	}

	/**
	 * @return int
	 */
	public function getPort() {
		return $this->protocol_port;
	}

	/**
	 * @return string
	 */
	public function getUsername() {
		return $this->protocol_username;
	}

	/**
	 * @return string
	 */
	public function getPassword() {
		return $this->protocol_password;
	}

	public function preparePath($filename) {
		return $this->applyPath($filename);
	}

	/**
	 * @return string
	 */
	public function getDefaultFilePermissions() {
		return $this->protocol_file_permissions ?? '0744';
	}

	/**
	 * @return \League\Flysystem\Adapter\Ftp
	 */
	public function getFlySystemAdapter() {
		return new \League\Flysystem\Adapter\Ftp([
			'host' => $this->getHostname(),
			'port' => $this->getPort(),
			'username' => $this->getUsername(),
			'password' => $this->getPassword(),
			'permPublic' => octdec($this->getDefaultFilePermissions())
		]);
	}

	public function getProperties() {
		return array_merge([[
			[
				'name' => 'Server hostname',
				'systemname' => 'protocol_hostname',
				'datatype_id' => 6,
				'definition' => [
					'mandatory' => true,
				],
			],
			[
				'name' => 'Server port',
				'systemname' => 'protocol_port',
				'datatype_id' => 5,
				'definition' => [
					'mandatory' => false,
				],
			],
			[
				'name' => 'Username',
				'systemname' => 'protocol_username',
				'datatype_id' => 6,
				'definition' => [
					'mandatory' => true,
				],
			],
			[
				'name' => 'Password',
				'systemname' => 'protocol_password',
				'datatype_id' => 6,
				'definition' => [
					'mandatory' => true,
				],
			],
			[
				'name' => 'Default file permissions',
				'systemname' => 'protocol_file_permissions',
				'datatype_id' => 6,
				'definition' => [
					'mandatory' => true,
					'defaultvalue' => '0744',
					'pattern' => '[0-7]{4}'
				],
			]
		]], parent::getProperties());
	}

	public function jsonSerialize() {
		$json = parent::jsonSerialize();
		$json->protocol_hostname = $this->getHostname();
		$json->protocol_port = $this->getPort();
		$json->protocol_username = $this->getUsername();
		$json->protocol_password = $this->getPassword();
		$json->protocol_file_permissions = $this->getDefaultFilePermissions();

		return $json;
	}
}
