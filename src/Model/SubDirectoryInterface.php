<?php

namespace QBNK\QBank\Protocol\Model;

interface SubDirectoryInterface {

	const SYSTEMNAME_SUBDIR = 'protocol_deploydir';

	/**
	 * @return string
	 */
	public function getSubDirectory();

	public function subDirectoryConstruct(array $params = null);

	public function getSubDirectoryProperties();

	public function getSubDirectoryJson(\stdClass $json);
}