<?php

namespace QBNK\QBank\Protocol\Model;

use League\Flysystem\Adapter\Local;

class Copy extends ProtocolAbstract implements DynamicSubDirectoryInterface, SubDirectoryInterface {
	use DynamicSubDirectoryTrait;
	use SubDirectoryTrait;

	const NAME =  'Copy';
	const DESCRIPTION = 'Copies the published files to another location on the filesystem.';

	public function __construct(array $parameters = []) {
		parent::__construct($parameters);

		$this->subDirectoryConstruct($parameters);
		$this->dynamicSubDirectoryConstruct($parameters);
	}

	public function getProperties() {
		return [
			array_merge(
				$this->getSubDirectoryProperties(),
				$this->getDynamicSubDirectoryProperties()
			)
		];
	}

	/**
	 * Adapter prefixes $filename internally so we need to return it un-prefixed before handling
	 * @param string $filename
	 * @return string
	 */
	public function preparePath($filename) {
		return ltrim(str_replace($this->getSubDirectory(), '', $filename), DIRECTORY_SEPARATOR);
	}

	public function applyPath($filename) {
		return rtrim($this->getSubDirectory(), DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.$filename;
	}

	/**
	 * @return Local
	 */
	public function getFlySystemAdapter() {
		return new Local($this->getSubDirectory(), LOCK_NB);
	}

	public function jsonSerialize() {
		$json = parent::jsonSerialize();
		$json = $this->getSubDirectoryJson($json);
		$json = $this->getDynamicSubDirectoryJson($json);
		return $json;
	}
}