<?php

namespace QBNK\QBank\Protocol\Model;

trait SubDirectoryTrait {

	/** @var string */
	protected $subDirectory;

	/**
	 * @return string
	 */
	public function getSubDirectory() {
		return $this->subDirectory;
	}

	public function subDirectoryConstruct(array $params = null) {
		if (isset($params[self::SYSTEMNAME_SUBDIR])) {
			$this->subDirectory = $params[self::SYSTEMNAME_SUBDIR];
		}
	}

	public function getSubDirectoryProperties() {
		return [
			[
				'name' => 'Subdirectory',
				'systemname' => self::SYSTEMNAME_SUBDIR,
				'datatype_id' => 6,
				'definition' => [
					'mandatory' => true,
				],
			]
		];
	}

	public function getSubDirectoryJson(\stdClass $json) {
		$json->{self::SYSTEMNAME_SUBDIR} = $this->getSubDirectory();
		return $json;
	}
}