<?php

namespace QBNK\QBank\Protocol\Model;

interface DynamicSubDirectoryInterface {

	const SYSTEMNAME_SUBDIR_DEPTH = 'subdirdepth';
	const SYSTEMNAME_SUBDIR_TYPE = 'subdirtype';
	const SUBDIR_TYPE_MEDIAID = 'Media ID';
	const SUBDIR_TYPE_HASH = 'Hash';

	/**
	 * @return string
	 */
	public function getSubDirectoryType();

	/**
	 * @param string $subDirectoryType
	 * @return $this
	 */
	public function setSubDirectoryType($subDirectoryType);

	/**
	 * @return int
	 */
	public function getSubDirectoryDepth();

	/**
	 * @param int $subDirectoryDepth
	 * @return DeployJob
	 */
	public function setSubDirectoryDepth($subDirectoryDepth);

	public function dynamicSubDirectoryConstruct(array $params = null);

	/**
	 * @return array
	 */
	public function getDynamicSubDirectoryProperties();


	public function getDynamicSubDirectoryJson(\stdClass $json);
}