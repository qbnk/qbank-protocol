<?php

namespace QBNK\QBank\Protocol\Model;

use League\Flysystem\Sftp\SftpAdapter;

class Sftp extends Ftp {
	const NAME = 'SFTP';
	const DESCRIPTION = 'Connects to an SFTP-server and transfers the published files there.';

	/**
	 * @var string
	 */
	protected $privateKey;

	/** @var string */
	protected $protocol_folder_permissions;

	public function __construct(array $parameters = []) {
		parent::__construct($parameters);

		if (isset($parameters['protocol_privatekey'])) {
			$this->privateKey = $parameters['protocol_privatekey'];
		}
		if (isset($parameters['protocol_folder_permissions'])) {
			$this->protocol_folder_permissions = $parameters['protocol_folder_permissions'];
		}
	}

	public function getProperties() {
		return array_merge([[
			[
				'name' => 'Private key',
				'systemname' => 'protocol_privatekey',
				'datatype_id' => 6,
				'definition' => [
					'multiline' => true,
				]
			],
			[
				'name' => 'Default folder permissions',
				'systemname' => 'protocol_folder_permissions',
				'datatype_id' => 6,
				'definition' => [
					'mandatory' => true,
					'defaultvalue' => '0744',
					'pattern' => '[0-7]{4}'
				],
			]
		]], parent::getProperties());
	}

	/**
	 * @return int
	 */
	public function getDefaultFolderPermissions() {
		return $this->protocol_folder_permissions ?? '0744';
	}

	/**
	 * @return SftpAdapter
	 */
	public function getFlySystemAdapter() {
		return new SftpAdapter([
			'host' => $this->getHostname(),
			'port' => $this->getPort(),
			'username' => $this->getUsername(),
			'password' => $this->getPassword(),
			'privateKey' => $this->getPrivateKey(),
			'permPublic' => octdec($this->getDefaultFilePermissions()),
			'directoryPerm' => octdec($this->getDefaultFolderPermissions())
		]);
	}

	/**
	 * @return string
	 */
	public function getPrivateKey() {
		return $this->privateKey;
	}

	public function jsonSerialize() {
		$json = parent::jsonSerialize();
		$json->protocol_privatekey = $this->getPrivateKey();
		$json->protocol_folder_permissions = $this->getDefaultFolderPermissions();
		return $json;
	}
}