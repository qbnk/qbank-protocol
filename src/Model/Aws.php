<?php

namespace QBNK\QBank\Protocol\Model;

use Aws\S3\S3Client;
use League\Flysystem\AwsS3v3\AwsS3Adapter;

class Aws extends Copy {
	const NAME = 'AWS S3';
	const DESCRIPTION = 'Publish files to an Amazon S3 bucket';

	/**
	 * https://docs.aws.amazon.com/aws-sdk-php/v3/api/index.html
	 */
	const API_VERSION = '2006-03-01';

	/**
	 * @var string
	 */
	protected $accountKey;

	/**
	 * @var string
	 */
	protected $accountSecret;

	/**
	 * @var string
	 */
	protected $region;

	/**
	 * @var string
	 */
	protected $bucket;

	public function __construct(array $parameters = []) {
		parent::__construct($parameters);

		if (isset($parameters['protocol_accountsecret'])) {
			$this->accountSecret = $parameters['protocol_accountsecret'];
		}
		if (isset($parameters['protocol_accountkey'])) {
			$this->accountKey = $parameters['protocol_accountkey'];
		}
		if (isset($parameters['protocol_bucket'])) {
			$this->bucket = $parameters['protocol_bucket'];
		}
		if (isset($parameters['protocol_region'])) {
			$this->region = $parameters['protocol_region'];
		}
	}

	public function validateConnection() {
		try {
			$this->getS3Client()->headBucket(['Bucket' => $this->getBucket()]);
		} catch (\Exception $e) {
			return $e->getMessage();
		}

		return true;
	}

	public function getProperties() {
		return array_merge([
				[
					[
						'name' => 'Access key',
						'systemname' => 'protocol_accountkey',
						'datatype_id' => 6,
						'definition' => [
							'mandatory' => true,
						],
					],
					[
						'name' => 'Access secret',
						'systemname' => 'protocol_accountsecret',
						'datatype_id' => 6,
						'definition' => [
							'mandatory' => true,
						],
					],
					[
						'name' => 'Bucket',
						'systemname' => 'protocol_bucket',
						'datatype_id' => 6,
						'definition' => [
							'mandatory' => true,
						],
					],
					[
						'name' => 'Region',
						'systemname' => 'protocol_region',
						'datatype_id' => 6,
						'definition' => [
							'mandatory' => true,
							'array' => true,
							'options' => $this->getRegions()
						],
					],
				],
			],
			parent::getProperties());
	}

	public function getRegions() {
		return array_map(function($region) {
			return ['key' => $region[1], 'value' => $region[0]];
		}, [
			['US East (Ohio)', 'us-east-2'],
			['US East (N. Virginia)', 'us-east-1'],
			['US West (N. California)', 'us-west-1'],
			['US West (Oregon)', 'us-west-2'],
			['Asia Pacific (Tokyo)', 'ap-northeast-1'],
			['Asia Pacific (Seoul)', 'ap-northeast-2'],
			['Asia Pacific (Osaka-Local)', 'ap-northeast-3'],
			['Asia Pacific (Mumbai)', 'ap-south-1'],
			['Asia Pacific (Singapore)', 'ap-southeast-1'],
			['Asia Pacific (Sydney)', 'ap-southeast-2'],
			['Canada (Central)', 'ca-central-1'],
			['China (Beijing)', 'cn-north-1'],
			['China (Ningxia)', 'cn-northwest-1'],
			['EU (Frankfurt)', 'eu-central-1'],
			['EU (Ireland)', 'eu-west-1'],
			['EU (London)', 'eu-west-2'],
			['EU (Paris)', 'eu-west-3'],
			['South America (Sao Paulo)', 'sa-east-1']
		]);
	}

	/**
	 * @return string
	 */
	public function getAccountKey() {
		return $this->accountKey;
	}

	/**
	 * @return string
	 */
	public function getAccountSecret() {
		return $this->accountSecret;
	}

	/**
	 * @return string
	 */
	public function getRegion() {
		return $this->region;
	}

	/**
	 * @return string
	 */
	public function getBucket() {
		return $this->bucket;
	}

	/**
	 * @return AwsS3Adapter
	 */
	public function getFlySystemAdapter() {
		return new AwsS3Adapter($this->getS3Client(), $this->getBucket(), $this->getSubDirectory());
	}

	private function getS3Client() {
		return S3Client::factory([
			'credentials' => [
				'key'    => $this->getAccountKey(),
				'secret' => $this->getAccountSecret(),
			],
			'region' => $this->getRegion(),
			'version' => self::API_VERSION
		]);
	}

	public function jsonSerialize() {
		$json = parent::jsonSerialize();
		$json->protocol_accountsecret = $this->getAccountSecret();
		$json->protocol_accountkey = $this->getAccountKey();
		$json->protocol_bucket = $this->getBucket();
		$json->protocol_region = $this->getRegion();
		return $json;
	}
}