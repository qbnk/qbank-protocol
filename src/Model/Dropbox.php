<?php

namespace QBNK\QBank\Protocol\Model;

use GuzzleHttp\Exception\ClientException;
use Srmklive\Dropbox\Adapter\DropboxAdapter;
use Srmklive\Dropbox\Client\DropboxClient;

class Dropbox extends Copy {
	const NAME = 'Dropbox';
	const DESCRIPTION = 'Publish files to a Dropbox account';

	/**
	 * @var string
	 */
	protected $authToken;

	public function __construct(array $parameters = []) {
		parent::__construct($parameters);

		if (isset($parameters['protocol_authtoken'])) {
			$this->authToken = $parameters['protocol_authtoken'];
		}
	}

	public function preparePath($filename) {
		return $this->applyPath($filename);
	}

	public function validateConnection() {
		try {
			$this->getClient()->getAccountInfo();
		} catch (ClientException $e) {
			return $e->getMessage();
		}

		return true;
	}

	public function getProperties() {
		return array_merge([[
			[
				'name' => 'Authorization token',
				'description' => 'A token can be generated in the App Console for any Dropbox API app. You’ll find more info at the Dropbox Developer Blog.',
				'systemname' => 'protocol_authtoken',
				'datatype_id' => 6,
				'definition' => [
					'mandatory' => true,
				],
			]
		]], parent::getProperties());
	}

	/**
	 * @return string
	 */
	public function getAuthorizationToken() {
		return $this->authToken;
	}

	/**
	 * @return DropboxAdapter
	 */
	public function getFlySystemAdapter() {
		return new DropboxAdapter($this->getClient());
	}

	public function jsonSerialize() {
		$json = parent::jsonSerialize();
		$json->protocol_authtoken = $this->getAuthorizationToken();
		return $json;
	}

	private function getClient() {
		return new DropboxClient($this->getAuthorizationToken());
	}
}